package com.example.baris.mapapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class EtiketRecyclerViewAdapter extends RecyclerView.Adapter<EtiketRecyclerViewAdapter.MyViewHolder> {

    private List<Etiket> etiketList;
    private static final int RESULT_FAV = 4;


    class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView lblEtiketAdi;

        public MyViewHolder(View view) {
            super(view);
            lblEtiketAdi = view.findViewById(R.id.lblEtiletAdi);
        }
    }

    public EtiketRecyclerViewAdapter(List<Etiket> etiketList) {
        this.etiketList = etiketList;
    }

    @Override
    public EtiketRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.element_etiket, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EtiketRecyclerViewAdapter.MyViewHolder holder, int position) {
        Etiket etiket = etiketList.get(position);
        holder.lblEtiketAdi.setText(etiket.getIsim());
    }

    @Override
    public int getItemCount() {
        return etiketList.size();
    }

    public Etiket getItemAt(int position) {
        return etiketList.get(position);
    }
}