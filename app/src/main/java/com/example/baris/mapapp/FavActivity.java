package com.example.baris.mapapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class FavActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private EtiketDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_markers);

        initComponents();
        bindData();
    }

    private void bindData() {

        List<Etiket> etiketList = dao.loadAllEtikets();
        ArrayList<Etiket> etiketler = new ArrayList<>(etiketList);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setLayoutParams(params);
        recyclerView.setHasFixedSize(true);

        final EtiketRecyclerViewAdapter adapter = new EtiketRecyclerViewAdapter(etiketler);
        recyclerView.setAdapter(adapter);

        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                Etiket etiket = adapter.getItemAt(position);
                Intent returnIntent = new Intent();
                returnIntent.putExtra("etiketId", etiket.getId());
                setResult(RESULT_OK, returnIntent);
                finish();
            }

            @Override
            public void onLongItemClick(View view, int position) {
            }
        }));
    }

    private void initComponents() {
        recyclerView = findViewById(R.id.recyclerView);

        AppDatabase appDatabase = AppDatabase.getAppDatabase(this);
        dao = appDatabase.getEtiketDAO();
    }
}
