package com.example.baris.mapapp;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import com.google.android.gms.maps.model.LatLng;

@Entity(tableName = "Etiket",
        indices = {@Index(value = {"enlem", "boylam"}, unique = true)})
public class Etiket {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "isim")
    private String isim;

    @ColumnInfo(name = "enlem")
    private Double enlem;

    @ColumnInfo(name = "boylam")
    private Double boylam;

    public Etiket() {
    }

    public Etiket(String isim, Double enlem, Double boylam) {
        this.isim = isim;
        this.enlem = enlem;
        this.boylam = boylam;
    }

    public LatLng getLatLng() {
        return new LatLng(enlem, boylam);
    }

    public String getIsim() {
        return isim;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public Double getEnlem() {
        return enlem;
    }

    public void setEnlem(Double enlem) {
        this.enlem = enlem;
    }

    public Double getBoylam() {
        return boylam;
    }

    public void setBoylam(Double boylam) {
        this.boylam = boylam;
    }

    @Override
    public String toString() {
        return isim;
    }
}
