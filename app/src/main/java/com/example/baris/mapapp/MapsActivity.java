package com.example.baris.mapapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap map;
    private EtiketDAO dao;

    private Button btnSearch, btnFav;
    private EditText txtSearch;
    private FloatingActionButton fab;

    private LinearLayout mapLayout;

    private static final int REQUEST_LOCATION = 1;
    private static final int REQUEST_SAVE_MARKER = 2;
    private static final int REQUEST_CALCULATE_DISTANCE = 3;
    private static final int REQUEST_FAV = 4;
    private static final int RESULT_DELETE = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_Launcher);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initComponents();
        registerEventHandlers();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        setUpMap(googleMap);
        updateMap();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SAVE_MARKER) {
            if (resultCode == Activity.RESULT_OK) {
                int etiketId = data.getIntExtra("etiketId", 0);
                Etiket e = dao.loadEtiketById(etiketId);
                updateMap();

                map.addMarker(new MarkerOptions().position(e.getLatLng()).title(e.getIsim())).showInfoWindow();
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(e.getLatLng(), 15));

                String msg = e.getIsim() + " başarılı bir şekilde kaydedildi.";
                Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this, "Düzenleme işlemi iptal edildi.", Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_DELETE) {
                updateMap();
                Toast.makeText(this, "Etiket başarıyla silindi.", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == REQUEST_CALCULATE_DISTANCE) {
            if (resultCode == Activity.RESULT_OK) {
                updateMap();

                int baslangicId = data.getIntExtra("baslangicId", 0);
                int bitisId = data.getIntExtra("bitisId", 0);
                String mode = data.getStringExtra("mode");

                Etiket e1 = dao.loadEtiketById(baslangicId);
                Etiket e2 = dao.loadEtiketById(bitisId);

                String url = getRequestUrl(e1.getLatLng(), e2.getLatLng(), mode);
                TaskRequestDirections taskRequestDirections = new TaskRequestDirections();
                taskRequestDirections.execute(url);

                float results[] = new float[10];
                Location.distanceBetween(e1.getEnlem(), e1.getBoylam(), e2.getEnlem(), e2.getBoylam(), results);

                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(e2.getLatLng());
                markerOptions.title(e2.getIsim() + " - Bitiş Noktası");
                markerOptions.snippet("Mesafe = " + results[0] + " m");
                map.addMarker(markerOptions).showInfoWindow();
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(e2.getLatLng(), 15));
            }
        } else if (requestCode == REQUEST_FAV) {
            if (resultCode == Activity.RESULT_OK) {
                int etiketId = data.getIntExtra("etiketId", 0);
                Etiket etiket = dao.loadEtiketById(etiketId);

                map.addMarker(new MarkerOptions().position(etiket.getLatLng()).title(etiket.getIsim())).showInfoWindow();
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(etiket.getLatLng(), 15));
            }
        }
    }

    private void updateMap() {
        map.clear();

        List<Etiket> etikets = dao.loadAllEtikets();
        for (Etiket e : etikets) {
            LatLng latLng = new LatLng(e.getEnlem(), e.getBoylam());
            map.addMarker(new MarkerOptions().position(latLng).title(e.getIsim()));
        }
    }

    private void btnSearch_onClick() {
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String location = txtSearch.getText().toString();
                List<Address> addressList = null;

                if (!location.equals("")) {
                    Geocoder geocoder = new Geocoder(MapsActivity.this);
                    try {
                        addressList = geocoder.getFromLocationName(location, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (addressList != null && addressList.size() > 0) {
                    Address address = addressList.get(0);
                    LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                    map.addMarker(new MarkerOptions().position(latLng).title(location)).showInfoWindow();
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

                } else {
                    Toast.makeText(MapsActivity.this, "Aradığınız lokasyon bulunamadı.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void btnFav_onClick(){
        btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsActivity.this, FavActivity.class);
                startActivityForResult(intent, REQUEST_FAV);
            }
        });
    }

    private void floatingButton_onClick() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsActivity.this, MesafeActivity.class);
                startActivityForResult(intent, REQUEST_CALCULATE_DISTANCE);
            }
        });
    }

    private void onMapClickListener() {
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {
                map.addMarker(new MarkerOptions().position(latLng).title("İsimsiz"));
            }
        });
    }

    private void onMarkerClickListener() {
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                final double enlem = marker.getPosition().latitude;
                final double boylam = marker.getPosition().longitude;

                String title = "İsimsiz";
                final Etiket e = dao.loadEtiketByKoordinat(enlem, boylam);
                if (e != null) {
                    title = e.getIsim();
                    marker.setTitle(title);
                }

                final String isim = title;
                marker.showInfoWindow();

                LatLng latLng = new LatLng(enlem, boylam);
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

                Snackbar snackbar = Snackbar.make(mapLayout, marker.getTitle(), Snackbar.LENGTH_LONG);
                snackbar.setAction("Düzenle", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MapsActivity.this, KaydetActivity.class);
                        intent.putExtra("isim", isim);
                        intent.putExtra("enlem", enlem);
                        intent.putExtra("boylam", boylam);
                        startActivityForResult(intent, REQUEST_SAVE_MARKER);
                    }
                });
                snackbar.show();

                return true;
            }
        });
    }

    private void setUpMap(GoogleMap googleMap) {
        map = googleMap;
        map.getUiSettings().setZoomControlsEnabled(true);

        onMapClickListener();
        onMarkerClickListener();
    }

    private void registerEventHandlers() {
        btnSearch_onClick();
        btnFav_onClick();
        floatingButton_onClick();
    }

    private void initComponents() {
        mapLayout = findViewById(R.id.mapLayout);
        btnSearch = findViewById(R.id.btnSearch);
        btnFav = findViewById(R.id.btnMarkers);
        txtSearch = findViewById(R.id.txtSearch);
        fab = findViewById(R.id.fab);

        AppDatabase appDatabase = AppDatabase.getAppDatabase(this);
        dao = appDatabase.getEtiketDAO();
    }

    private String getRequestUrl(LatLng origin, LatLng dest, String mode) {

        String str_org = "origin=" + origin.latitude +","+origin.longitude;

        String str_dest = "destination=" + dest.latitude+","+dest.longitude;

        String sensor = "sensor=false";

        mode = "mode=" + mode;

        String param = str_org +"&" + str_dest + "&" +sensor+"&" +mode;

        String output = "json";

        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + param;
    }

    private String requestDirection(String reqUrl) throws IOException {
        String responseString = "";
        InputStream inputStream = null;
        HttpURLConnection httpURLConnection = null;
        try{
            URL url = new URL(reqUrl);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.connect();

            //Get the response result
            inputStream = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            StringBuffer stringBuffer = new StringBuffer();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
            }

            responseString = stringBuffer.toString();
            bufferedReader.close();
            inputStreamReader.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            httpURLConnection.disconnect();
        }
        return responseString;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    map.setMyLocationEnabled(true);
                }
                break;
        }
    }

    public class TaskRequestDirections extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            String responseString = "";
            try {
                responseString = requestDirection(strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return  responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Parse json here
            TaskParser taskParser = new TaskParser();
            taskParser.execute(s);
        }
    }

    public class TaskParser extends AsyncTask<String, Void, List<List<HashMap<String, String>>> > {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
            JSONObject jsonObject = null;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jsonObject = new JSONObject(strings[0]);
                DirectionsParser directionsParser = new DirectionsParser();
                routes = directionsParser.parse(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
            //Get list route and display it into the map

            ArrayList points;

            PolylineOptions polylineOptions = null;

            for (List<HashMap<String, String>> path : lists) {
                points = new ArrayList();
                polylineOptions = new PolylineOptions();

                for (HashMap<String, String> point : path) {
                    double lat = Double.parseDouble(point.get("lat"));
                    double lon = Double.parseDouble(point.get("lon"));

                    points.add(new LatLng(lat,lon));
                }

                polylineOptions.addAll(points);
                polylineOptions.width(15);
                polylineOptions.color(Color.BLUE);
                polylineOptions.geodesic(true);
            }

            if (polylineOptions!=null) {
                map.addPolyline(polylineOptions);
            } else {
                Toast.makeText(getApplicationContext(), "Rota bulunamadı!", Toast.LENGTH_SHORT).show();
            }

        }
    }

}
