package com.example.baris.mapapp;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface EtiketDAO {

    @Insert
    void insertEtiket(Etiket etiket);

    @Update
    void updateEtiket(Etiket etiket);

    @Delete
    void deleteEtiket(Etiket etiket);

    @Query("SELECT * FROM Etiket WHERE enlem=:enlem AND boylam=:boylam")
    boolean existsEtiket(double enlem, double boylam);

    @Query("SELECT * FROM Etiket")
    List<Etiket> loadAllEtikets();

    @Query("SELECT * FROM Etiket WHERE enlem=:enlem AND boylam=:boylam")
    Etiket loadEtiketByKoordinat(double enlem, double boylam);

    @Query("SELECT * FROM Etiket WHERE id=:id")
    Etiket loadEtiketById(int id);
}
