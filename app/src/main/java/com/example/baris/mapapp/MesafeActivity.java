package com.example.baris.mapapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MesafeActivity extends AppCompatActivity {

    private Spinner spinnerBaslangic, spinnerBitis;
    private RadioGroup rdUlasim;
    private Button btnHesapla, btnIptal;
    private EtiketDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mesafe);

        initComponents();
        registerEventHandlers();
    }

    private void loadSpinnerData(Spinner spinner) {
        List<Etiket> etikets = dao.loadAllEtikets();

        ArrayList<Etiket> etiketArrayList = new ArrayList<>(etikets);

        ArrayAdapter<Etiket> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, etiketArrayList);

        spinner.setAdapter(adapter);
    }

    private void spinnerBaslangic_onItemSelected() {
        spinnerBaslangic.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                validateSpinners();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void spinnerBitis_onItemSelected() {
        spinnerBitis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                validateSpinners();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void validateSpinners() {
        Etiket secilenBaslangic = (Etiket) spinnerBaslangic.getSelectedItem();
        Etiket secilenBitis = (Etiket) spinnerBitis.getSelectedItem();

        if (secilenBaslangic.getId() == secilenBitis.getId()) {
            TextView errorText = (TextView) spinnerBitis.getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);
            errorText.setText("Başlangıç ve bitiş noktalarını farklı seçiniz!");
            btnHesapla.setClickable(false);
        } else {
            btnHesapla.setClickable(true);
        }
    }

    private void btnHesapla_onClick() {
        btnHesapla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Etiket origin = (Etiket) spinnerBaslangic.getSelectedItem();
                Etiket dest = (Etiket) spinnerBitis.getSelectedItem();
                String radioText = getSelectedRadio();

                String mode;
                switch (radioText) {
                    case "Araba":
                        mode = "driving";
                        break;
                    case "Yaya":
                        mode = "walking";
                        break;
                    default:
                        mode = "bicycling";
                        break;
                }

                Intent returnIntent = new Intent();
                returnIntent.putExtra("baslangicId", origin.getId());
                returnIntent.putExtra("bitisId", dest.getId());
                returnIntent.putExtra("mode", mode);
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    private String getSelectedRadio() {
        int radioButtonID = rdUlasim.getCheckedRadioButtonId();
        View selectedRadio = rdUlasim.findViewById(radioButtonID);
        int idx = rdUlasim.indexOfChild(selectedRadio);

        RadioButton r = (RadioButton) rdUlasim.getChildAt(idx);
        return r.getText().toString();
    }

    private void btnIptal_onClick() {
        btnIptal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    private void registerEventHandlers() {
        btnHesapla_onClick();
        btnIptal_onClick();
        spinnerBaslangic_onItemSelected();
        spinnerBitis_onItemSelected();
    }

    private void initComponents() {
        spinnerBaslangic = findViewById(R.id.spinnerBaslangic);
        spinnerBitis = findViewById(R.id.spinnerBitis);
        rdUlasim = findViewById(R.id.radioUlasim);
        btnHesapla = findViewById(R.id.btnHesapla);
        btnIptal = findViewById(R.id.btnHesaplaIptal);

        AppDatabase appDatabase = AppDatabase.getAppDatabase(this);
        dao = appDatabase.getEtiketDAO();

        loadSpinnerData(spinnerBaslangic);
        loadSpinnerData(spinnerBitis);
        spinnerBitis.setSelection(1);
    }
}
