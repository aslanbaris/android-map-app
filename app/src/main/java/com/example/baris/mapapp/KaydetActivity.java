package com.example.baris.mapapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class KaydetActivity extends AppCompatActivity {

    Button btnKaydet, btnIptal, btnSil;
    EditText txtMarker;
    Double enlem, boylam;
    private EtiketDAO dao;
    private static final int RESULT_DELETE = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kaydet);

        initComponents();
        registerEventHandlers();
    }

    private void btnKaydet_onClick() {
        btnKaydet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String isim = txtMarker.getText().toString();
                Etiket etiket = new Etiket(isim, enlem, boylam);

                if (dao.existsEtiket(enlem, boylam)) {
                    etiket = dao.loadEtiketByKoordinat(enlem, boylam);
                    etiket.setIsim(isim);
                    dao.updateEtiket(etiket);
                } else {
                    dao.insertEtiket(etiket);
                }

                Intent returnIntent = new Intent();
                returnIntent.putExtra("etiketId", etiket.getId());
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    private void btnIptal_onClick() {
        btnIptal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    private void btnSil_onClick() {
        btnSil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Etiket etiket = dao.loadEtiketByKoordinat(enlem, boylam);

                if (etiket != null) {
                    dao.deleteEtiket(etiket);
                }

                setResult(RESULT_DELETE);
                finish();
            }
        });
    }

    private void registerEventHandlers() {
        btnIptal_onClick();
        btnKaydet_onClick();
        btnSil_onClick();
    }

    private void initComponents() {
        btnKaydet = findViewById(R.id.btnKaydet);
        btnIptal = findViewById(R.id.btnIptal);
        btnSil = findViewById(R.id.btnSil);
        txtMarker = findViewById(R.id.txtMarker);

        Intent i = getIntent();
        txtMarker.setText(i.getStringExtra("isim"));
        enlem = i.getDoubleExtra("enlem", 0);
        boylam = i.getDoubleExtra("boylam", 0);

        AppDatabase appDatabase = AppDatabase.getAppDatabase(this);
        dao = appDatabase.getEtiketDAO();
    }
}
